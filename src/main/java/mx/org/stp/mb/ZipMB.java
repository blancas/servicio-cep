package mx.org.stp.mb; 
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import lombok.Getter;
import lombok.Setter;
import mx.org.stp.controller.ZipController;
import mx.org.stp.entity.SPEIBean;
import mx.org.stp.entity.Ordenante;
import mx.org.stp.entity.Beneficiario;
import mx.org.stp.entity.ResponseBean;
import mx.org.stp.entity.Zip;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author luisa
 */ 
@ManagedBean (name = "zipMB")
@RequestScoped
@Getter
@Setter
public class ZipMB
{
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<Zip> todos; 
    private Zip bean;
    private ZipController controller;
    private int     id;
    private String  nombre;
    private int    pdf;
    private int    xml;
    @PostConstruct
    public void init() {
        bean=new Zip();
        controller=new ZipController();
    }
    private UploadedFile file;
 
    public UploadedFile getFile() {
        return file;
    }
 
    public void setFile(UploadedFile file) {
        this.file = file;
    }
     
    public void upload() 
    { 
        ExternalContext contextEx = FacesContext.getCurrentInstance().getExternalContext();
        Object requestObj = contextEx.getRequest();
        HttpServletRequest httpRequest =null;
        if(requestObj instanceof HttpServletRequest)
            httpRequest = (HttpServletRequest)requestObj;
        String realPath=""; 
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        realPath=(String) servletContext.getRealPath("/"); 
        System.out.println("dato realPath "+realPath);
        if(file != null) 
        {
            String carpeta=""+(new Date()).getTime();
            File createCarpeta=new File(realPath+carpeta);
            if(!createCarpeta.isDirectory())
                createCarpeta.mkdir();
            try {
                String nombre = ""+(new Date()).getTime()+".zip";
                if(moveFile(file.getInputstream(), nombre,createCarpeta.getAbsolutePath()+ (File.separator) ))
                {
                    FacesMessage message = new FacesMessage("Exito","Se guardo archivo :: "
                            + (createCarpeta.getAbsolutePath()+File.separator+nombre) + " , con exito.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    String carpetaFinal =createCarpeta.getAbsolutePath()+File.separator;
                    String archivo =createCarpeta.getAbsolutePath()+File.separator+nombre;
                    creArchivosXML_PDF(archivo,carpetaFinal);
                    File pdf_xml = new File(carpetaFinal);
                    int conteoXML =cuantosXMLExiste(pdf_xml,".XML");
                    int conteoPDF =cuantosXMLExiste(pdf_xml,".PDF");
                    message = new FacesMessage("PDF","Conteo de PDF :: "+conteoPDF);
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    
                    message = new FacesMessage("XML","Conteo de XML :: "+conteoXML);
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    List<SPEIBean> lista=new ArrayList();
                    try {
                        lista =parsearXML(pdf_xml);
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    ResponseBean[] array=converterListToArray(lista);
                    String jsonArray = getJson(array);
                    String jsonList  = getJson(lista);
                    System.out.println(" jsonArray   "+jsonArray);
                    System.out.println(" jsonList   "+jsonList);
                    String resultado =invokeServiceWeb(jsonArray);
                    System.out.println(" resultado   "+resultado);
                    Zip zip = controller.cargaZIP(archivo,conteoPDF,conteoXML,lista.size(),1);
                    message = new FacesMessage("Parseando XML","Numero de archivos parseados :: "+lista.size());
                    FacesContext.getCurrentInstance().addMessage(null, message);
                }
            } catch (IOException e1) {
                System.out.println(e1);
            }
        }
    }
    private String getJson(List<SPEIBean> lista)
    { 
        Gson gson = new Gson();  
        String dataJson = gson.toJson(lista); 
        return dataJson;
    }
    private String getJson(ResponseBean lista[] )
    { 
        Gson gson = new Gson();  
        String dataJson = gson.toJson(lista); 
        return dataJson;
    }
    private ResponseBean[] converterListToArray(List<SPEIBean> lista)
    { 
        ResponseBean array[]= new ResponseBean[lista.size()];
        int cont=0;
        for(SPEIBean bean:lista)
            array[cont++]=new ResponseBean(
                    bean.getFechaOperacion(),
                    bean.getClaveRastreo(),
                    bean.getOrdenante().getBancoEmisor(),
                    bean.getBeneficiario().getBancoReceptor(),
                    bean.getBeneficiario().getMontoPago(),
                    bean.getBeneficiario().getRfc(),
                    bean.getBeneficiario().getNombre());
        return array;
    }
    
    private String invokeServiceWeb(String obj)
    {
        String resultado="";
        String url="https://demo.stpmex.com:7024/conciliadorcep/conciliaDatosCep";
        RestTemplate restTemplate = new RestTemplate();
        resultado= restTemplate.postForObject(url, obj, String.class);
        System.out.println(" result  "+resultado);
        return resultado;
    }
    public void creArchivosXML_PDF(String fileZip,String pathRuta) 
    {
        ZipInputStream zis = null;
        try {
            File destDir = new File(pathRuta);
            byte[] buffer = new byte[1024];
            zis = new ZipInputStream(new FileInputStream(fileZip));
            ZipEntry zipEntry = null;
            try {
                zipEntry = zis.getNextEntry();
            } catch (IOException ex) {
                Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
            }
            while (zipEntry != null)
            {
                File newFile = null;
                try {
                    newFile = newFile(destDir, zipEntry);
                } catch (IOException ex) {
                    Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
                }
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                try {
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    fos.close();
                } catch (IOException ex) {
                    Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    zipEntry = zis.getNextEntry();
                } catch (IOException ex) {
                    Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }  try {
                zis.closeEntry();
            } catch (IOException ex) {
                Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                zis.close();
            } catch (IOException ex) {
                Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                zis.close();
            } catch (IOException ex) {
                Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
     
    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());
         
        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();
         
        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }
         
        return destFile;
    }
    
    private Boolean moveFile(InputStream inputStream, String name,String path) 
    {

        OutputStream outputStream = null; 
        Boolean flag = false;
        try {
            outputStream
                    = new FileOutputStream(new File(path + name));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            System.out.println("Done!");
            flag = true;
        } catch (IOException e) {
           flag = false;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        return flag;
    }
    public void handleFileUpload(FileUploadEvent event) {
        FacesMessage msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    public List<Zip> getTodos()
    {    
        this.todos= controller.getAll();
        return this.todos;
    } 
    public void seleccionEditar(Zip editar )
    {
        this.bean=new Zip();
        this.bean=edicion(editar); 
        System.out.println("seleccion Editar (seleccionEditar) "+editar.toString());
    } 
    public Zip edicion(Zip editar )
    {
        Zip b=new Zip();
        b.setId(editar.getId());
        b.setNombre(editar.getNombre());
        b.setPdf(editar.getPdf());
        b.setXml(editar.getXml());
        return b;
    }  

    private int cuantosXMLExiste(File pdf_xml,String extension) 
    {
        int total=0;
        for(int conteo=0;conteo<pdf_xml.listFiles().length;conteo++)
        {
            String archivo = (pdf_xml.listFiles())[conteo].getName().toUpperCase();
            if(archivo.indexOf(extension)>0)
                total++;
        }
        return total;
    }
    private List<SPEIBean> parsearXML(File pdf_xml) throws ParserConfigurationException
    { 
        List<SPEIBean> lista = new ArrayList<SPEIBean>();
        for(int conteo=0;conteo<pdf_xml.listFiles().length;conteo++)
        {
            if(((pdf_xml.listFiles())[conteo]).getName().toUpperCase().indexOf(".XML")>0)
            {
                try 
                {
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(((pdf_xml.listFiles())[conteo]));
                    doc.getDocumentElement().normalize();
                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    NodeList nList = doc.getElementsByTagName("SPEI_Tercero");
                    System.out.println("----------------------------");
                    for (int temp = 0; temp < nList.getLength(); temp++)
                    {
                        Node nNode = nList.item(temp);
                        System.out.println("\nCurrent Element :" + nNode.getNodeName());
                        if (nNode.getNodeType() == Node.ELEMENT_NODE) 
                        {
                            Element eElement = (Element) nNode;
                            SPEIBean spei = new SPEIBean();
                            spei.setFechaOperacion(eElement.getAttribute("FechaOperacion"));
                            spei.setClaveRastreo(eElement.getAttribute("ClaveSPEI"));
                            spei.setSello(eElement.getAttribute("sello"));
                            spei.setClaveRastreo(eElement.getAttribute("claveRastreo")); 
                            
                            NodeList benfOrd = eElement.getChildNodes();
                            for(int cont =0; cont < benfOrd.getLength() ;  cont ++)
                            {
                                Node child = benfOrd.item(cont);
                                NamedNodeMap attrs = child.getAttributes();
                                
                                String local= child.getNodeName();
                                if(local.equals("Beneficiario"))
                                {
                                    System.out.println("\tBeneficiario");
                                    Beneficiario ben=new Beneficiario();
                                    ben.setBancoReceptor(attrs.getNamedItem("BancoReceptor").getNodeValue());
                                    ben.setNombre( attrs.getNamedItem("Nombre").getNodeValue());
                                    ben.setTipoCuenta(attrs.getNamedItem("TipoCuenta").getNodeValue());
                                    ben.setCuenta(attrs.getNamedItem("Cuenta").getNodeValue());
                                    ben.setRfc(attrs.getNamedItem("RFC").getNodeValue());
                                    ben.setConcepto(attrs.getNamedItem("Concepto").getNodeValue());
                                    ben.setIva(attrs.getNamedItem("IVA").getNodeValue());
                                    ben.setMontoPago(attrs.getNamedItem("MontoPago").getNodeValue());
                                    spei.setBeneficiario(ben);
                                }
                                if(local.equals("Ordenante"))
                                {
                                    Ordenante ord = new Ordenante();
                                    System.out.println("\tOrdenante");
                                    ord.setBancoEmisor( attrs.getNamedItem("BancoEmisor").getNodeValue());
                                    ord.setNombre(attrs.getNamedItem("Nombre").getNodeValue());
                                    ord.setTipoCuenta(attrs.getNamedItem("TipoCuenta").getNodeValue());
                                    ord.setCuenta(attrs.getNamedItem("Cuenta").getNodeValue());
                                    ord.setRfc( attrs.getNamedItem("RFC").getNodeValue());
                                    spei.setOrdenante(ord);
                                }
                                
                            }
                            lista.add(spei);
                        }
                    }
                } catch (SAXException ex) {
                    Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ZipMB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return lista;
    }
    public String obtieneContenido(File archivo) throws FileNotFoundException, IOException 
    {
        String resultado="";
        String cadena="";
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) 
        {
            System.out.println(cadena);
            resultado  = resultado + cadena.trim();
        }
        b.close();
        return resultado;
    }
}
