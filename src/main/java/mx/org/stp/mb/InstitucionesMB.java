/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.org.stp.mb; 
import java.io.IOException;
import java.io.InputStream;
import java.util.List; 
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct; 
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import lombok.Getter;
import lombok.Setter;
import mx.org.stp.controller.InstitucionController;
import mx.org.stp.entity.InstitucionBean;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author luisa
 */ 
@ManagedBean (name = "bankMB")
@RequestScoped
@Getter
@Setter
public class InstitucionesMB
{ 
    private final FacesContext context = FacesContext.getCurrentInstance();
    private List<InstitucionBean> todos; 
    private InstitucionBean bean;
    private InstitucionController controller;
    private int    id;
    private String  clave;
    private String  corta;
    private String  nombre;
    private boolean activo;
    @PostConstruct
    public void init() {
        bean=new InstitucionBean();
        controller=new InstitucionController();
    }
    
    public List<InstitucionBean> getTodos()
    {    
        this.todos= controller.getAll();
        return this.todos;
    } 
    public void seleccionEditar(InstitucionBean editar )
    {
        this.bean=new InstitucionBean();
        this.bean=edicion(editar);
    }
    public InstitucionBean edicion(InstitucionBean editar )
    {
        this.bean=new InstitucionBean();
        this.bean.setId(editar.getId());
        this.bean.setNombre(editar.getNombre());
        this.bean.setCorta(editar.getCorta());
        this.bean.setClave(editar.getClave());
        return this.bean;
    } 
    public void editarUsuario(Integer id,String corta, String desc, String clave)
    {
        try
        { 
            controller.modifica(id,corta, desc, clave,1);
            context.getExternalContext().redirect("Lista.xhtml?faces-redirect=true");
        }
        catch (IOException ex) {
            Logger.getLogger(UsuarioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void AnexarUsuario()
    {
        //try
        {
            RequestContext reqcontext = RequestContext.getCurrentInstance();
            reqcontext.execute("PF('AnexarActivo').hide();");
            System.out.println("Editar Nombre:"+this.id); 
            System.out.println("Editar Activo:"+clave);
            System.out.println("Editar Activo:"+corta);
            System.out.println("Editar Activo:"+nombre);
            controller.crea(clave,corta,nombre,activo);
            //context.getExternalContext().redirect("Lista.xhtml?faces-redirect=true");
        }
        //catch (IOException ex) 
        {
            //Logger.getLogger(UsuarioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void borrar(int id)
    {
        try
        {
            System.out.println("seleccion Borrar "+id);
            controller.borrar(id);            
            context.getExternalContext().redirect("Lista.xhtml?faces-redirect=true");
            context.addMessage(null, new FacesMessage("Borrar", "Registro  borrado con exito :  "+id));
        }
        catch (IOException ex) {
            Logger.getLogger(UsuarioMB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
